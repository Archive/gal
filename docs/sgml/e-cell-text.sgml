<refentry id="ECellText">
<refmeta>
<refentrytitle>ECellText</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GAL Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>ECellText</refname><refpurpose>Text Cell renderer.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>

<synopsis>



typedef     <link linkend="ECellText">ECellText</link>;
<link linkend="char">char</link>*       (<link linkend="ECellTextFilter">*ECellTextFilter</link>)              (const <link linkend="void">void</link>*);
#define     <link linkend="E-CELL-TEXT-TYPE:CAPS">E_CELL_TEXT_TYPE</link>
<link linkend="ECell">ECell</link>*      <link linkend="e-cell-text-new">e_cell_text_new</link>                 (const <link linkend="char">char</link> *fontname,
                                             <link linkend="GtkJustification">GtkJustification</link> justify);

</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GtkObject">GtkObject</link>
   +----<link linkend="ECell">ECell</link>
         +----ECellText
</synopsis>

</refsect1>





<refsect1>
<title>Properties</title>
<synopsis>

  &quot;<link linkend="ECellText--strikeout-column">strikeout-column</link>&quot;     <link linkend="gint">gint</link>                  : Read / Write
  &quot;<link linkend="ECellText--bold-column">bold-column</link>&quot;          <link linkend="gint">gint</link>                  : Read / Write
  &quot;<link linkend="ECellText--color-column">color-column</link>&quot;         <link linkend="gint">gint</link>                  : Read / Write
  &quot;<link linkend="ECellText--text-filter">text-filter</link>&quot;          <link linkend="gpointer">gpointer</link>              : Read / Write
</synopsis>
</refsect1>



<refsect1>
<title>Description</title>
<para>
   The ECellText implements an <literal>ETable</literal> cell renderer (of type
   <literal>ECell</literal>).  This one in particular uses the extensible editing
   features of <literal>EText</literal> (which is a general canvas item for doing text
   editing). 
</para>

<para>
   You can use <literal>ECellText</literal> renderer objects when you want to display
   strings that are in your <literal>ETableModel</literal>.   
</para>

<para>
   The ETable widget typically takes care of creating these rendering
   objects for you when the type string has been specified.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="ECellText">ECellText</title>
<indexterm><primary>ECellText</primary></indexterm><programlisting>typedef struct {
	ECell parent;

	GtkJustification  justify;
	char             *font_name;

	double x, y;			/* Position at anchor */

	gulong pixel;			/* Fill color */

	/* Clip handling */
	char *ellipsis;                 /* The ellipsis characters.  NULL = "...". */

	guint use_ellipsis : 1;         /* Whether to use the ellipsis. */
	
	int strikeout_column;
	int bold_column;

	/* This column in the ETable should return a string specifying a color,
	   either a color name like "red" or a color spec like "rgb:F/0/0".
	   See the XParseColor man page for the formats available. */
	int color_column;

	ECellTextFilter filter;

	/* This stores the colors we have allocated. */
	GHashTable *colors;
} ECellText;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="ECellTextFilter">ECellTextFilter ()</title>
<indexterm><primary>ECellTextFilter</primary></indexterm><programlisting><link linkend="char">char</link>*       (*ECellTextFilter)              (const <link linkend="void">void</link>*);</programlisting>
<para>
   The type for a filtering function that will be invoked to provide
   filtered data at runtime from the model. 
</para><variablelist role="params">
<varlistentry><term><parameter>Param1</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="E-CELL-TEXT-TYPE:CAPS">E_CELL_TEXT_TYPE</title>
<indexterm><primary>E_CELL_TEXT_TYPE</primary></indexterm><programlisting>#define E_CELL_TEXT_TYPE        (e_cell_text_get_type ())
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="e-cell-text-new">e_cell_text_new ()</title>
<indexterm><primary>e_cell_text_new</primary></indexterm><programlisting><link linkend="ECell">ECell</link>*      e_cell_text_new                 (const <link linkend="char">char</link> *fontname,
                                             <link linkend="GtkJustification">GtkJustification</link> justify);</programlisting>
<para>
Creates a new ECell renderer that can be used to render strings that
that come from the model.  The value returned from the model is
interpreted as being a char *.
</para>
<para>
The ECellText object support a large set of properties that can be
configured through the Gtk argument system and allows the user to have
a finer control of the way the string is displayed.  The arguments supported
allow the control of strikeout, underline, bold, and color.
</para>
<para>
The arguments "strikeout_column", "underline_column", "bold_column"
and "color_column" set and return an integer that points to a
column in the model that controls these settings.  So controlling
the way things are rendered is achieved by having special columns
in the model that will be used to flag whether the text should be
rendered with strikeout, or bolded.  In the case of the
"color_column" argument, the column in the model is expected to
have a string that can be parsed by <link linkend="gdk-color-parse"><function>gdk_color_parse()</function></link>.</para>
<para>
   Creates a new <literal>ECellText</literal> renderer object.
</para><variablelist role="params">
<varlistentry><term><parameter>fontname</parameter>&nbsp;:</term>
<listitem><simpara> this param is no longer used, but left here for api stability
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>justify</parameter>&nbsp;:</term>
<listitem><simpara> Justification of the string in the cell.
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> an ECell object that can be used to render strings.
</simpara></listitem></varlistentry>
</variablelist></refsect2>

</refsect1>
<refsect1>
<title>Properties</title>
<refsect2><title><anchor id="ECellText--strikeout-column">The &quot;<literal>strikeout-column</literal>&quot; property</title>
<programlisting>  &quot;strikeout-column&quot;     <link linkend="gint">gint</link>                  : Read / Write</programlisting>
<para></para></refsect2>
<refsect2><title><anchor id="ECellText--bold-column">The &quot;<literal>bold-column</literal>&quot; property</title>
<programlisting>  &quot;bold-column&quot;          <link linkend="gint">gint</link>                  : Read / Write</programlisting>
<para></para></refsect2>
<refsect2><title><anchor id="ECellText--color-column">The &quot;<literal>color-column</literal>&quot; property</title>
<programlisting>  &quot;color-column&quot;         <link linkend="gint">gint</link>                  : Read / Write</programlisting>
<para></para></refsect2>
<refsect2><title><anchor id="ECellText--text-filter">The &quot;<literal>text-filter</literal>&quot; property</title>
<programlisting>  &quot;text-filter&quot;          <link linkend="gpointer">gpointer</link>              : Read / Write</programlisting>
<para></para></refsect2>

</refsect1>



<refsect1>
<title>See Also</title>
<para>
  <literal>ECell</literal>, <literal>ECellCheckbox</literal>, <literal>ECellToggle</literal>, <literal>ECellTree</literal>.
</para>
</refsect1>

</refentry>
