<refentry id="gal-e-table-header-utils">
<refmeta>
<refentrytitle>e-table-header-utils</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GAL Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>e-table-header-utils</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>

<synopsis>



<link linkend="int">int</link>         <link linkend="e-table-header-compute-height">e_table_header_compute_height</link>   (<link linkend="ETableCol">ETableCol</link> *ecol,
                                             <link linkend="GtkStyle">GtkStyle</link> *style,
                                             <link linkend="GdkFont">GdkFont</link> *font);
<link linkend="void">void</link>        <link linkend="e-table-header-draw-button">e_table_header_draw_button</link>      (<link linkend="GdkDrawable">GdkDrawable</link> *drawable,
                                             <link linkend="ETableCol">ETableCol</link> *ecol,
                                             <link linkend="GtkStyle">GtkStyle</link> *style,
                                             <link linkend="GdkFont">GdkFont</link> *font,
                                             <link linkend="GtkStateType">GtkStateType</link> state,
                                             <link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="GdkGC">GdkGC</link> *gc,
                                             <link linkend="int">int</link> x,
                                             <link linkend="int">int</link> y,
                                             <link linkend="int">int</link> width,
                                             <link linkend="int">int</link> height,
                                             <link linkend="int">int</link> button_width,
                                             <link linkend="int">int</link> button_height,
                                             <link linkend="ETableColArrow">ETableColArrow</link> arrow);
<link linkend="void">void</link>        <link linkend="e-table-draw-elided-string">e_table_draw_elided_string</link>      (<link linkend="GdkDrawable">GdkDrawable</link> *drawable,
                                             <link linkend="GdkFont">GdkFont</link> *font,
                                             <link linkend="GdkGC">GdkGC</link> *gc,
                                             <link linkend="int">int</link> x,
                                             <link linkend="int">int</link> y,
                                             const <link linkend="char">char</link> *str,
                                             <link linkend="int">int</link> max_width,
                                             <link linkend="gboolean">gboolean</link> center);
</synopsis>
</refsynopsisdiv>









<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="e-table-header-compute-height">e_table_header_compute_height ()</title>
<indexterm><primary>e_table_header_compute_height</primary></indexterm><programlisting><link linkend="int">int</link>         e_table_header_compute_height   (<link linkend="ETableCol">ETableCol</link> *ecol,
                                             <link linkend="GtkStyle">GtkStyle</link> *style,
                                             <link linkend="GdkFont">GdkFont</link> *font);</programlisting>
<para>
Computes the minimum height required for a table header button.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>ecol</parameter>&nbsp;:</term>
<listitem><simpara> Table column description.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>style</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>font</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> The height of the button, in pixels.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="e-table-header-draw-button">e_table_header_draw_button ()</title>
<indexterm><primary>e_table_header_draw_button</primary></indexterm><programlisting><link linkend="void">void</link>        e_table_header_draw_button      (<link linkend="GdkDrawable">GdkDrawable</link> *drawable,
                                             <link linkend="ETableCol">ETableCol</link> *ecol,
                                             <link linkend="GtkStyle">GtkStyle</link> *style,
                                             <link linkend="GdkFont">GdkFont</link> *font,
                                             <link linkend="GtkStateType">GtkStateType</link> state,
                                             <link linkend="GtkWidget">GtkWidget</link> *widget,
                                             <link linkend="GdkGC">GdkGC</link> *gc,
                                             <link linkend="int">int</link> x,
                                             <link linkend="int">int</link> y,
                                             <link linkend="int">int</link> width,
                                             <link linkend="int">int</link> height,
                                             <link linkend="int">int</link> button_width,
                                             <link linkend="int">int</link> button_height,
                                             <link linkend="ETableColArrow">ETableColArrow</link> arrow);</programlisting>
<para>
Draws a button suitable for a table header.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>drawable</parameter>&nbsp;:</term>
<listitem><simpara> Destination drawable.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>ecol</parameter>&nbsp;:</term>
<listitem><simpara> Table column for the header information.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>style</parameter>&nbsp;:</term>
<listitem><simpara> Style to use for drawing the button.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>font</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>state</parameter>&nbsp;:</term>
<listitem><simpara> State of the table widget.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>widget</parameter>&nbsp;:</term>
<listitem><simpara> The table widget.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>gc</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>x</parameter>&nbsp;:</term>
<listitem><simpara> Leftmost coordinate of the button.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>y</parameter>&nbsp;:</term>
<listitem><simpara> Topmost coordinate of the button.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>width</parameter>&nbsp;:</term>
<listitem><simpara> Width of the region to draw.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>height</parameter>&nbsp;:</term>
<listitem><simpara> Height of the region to draw.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>button_width</parameter>&nbsp;:</term>
<listitem><simpara> Width for the complete button.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>button_height</parameter>&nbsp;:</term>
<listitem><simpara> Height for the complete button.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>arrow</parameter>&nbsp;:</term>
<listitem><simpara> Arrow type to use as a sort indicator.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="e-table-draw-elided-string">e_table_draw_elided_string ()</title>
<indexterm><primary>e_table_draw_elided_string</primary></indexterm><programlisting><link linkend="void">void</link>        e_table_draw_elided_string      (<link linkend="GdkDrawable">GdkDrawable</link> *drawable,
                                             <link linkend="GdkFont">GdkFont</link> *font,
                                             <link linkend="GdkGC">GdkGC</link> *gc,
                                             <link linkend="int">int</link> x,
                                             <link linkend="int">int</link> y,
                                             const <link linkend="char">char</link> *str,
                                             <link linkend="int">int</link> max_width,
                                             <link linkend="gboolean">gboolean</link> center);</programlisting>
<para>
Draws a string, possibly trimming it so that it fits inside the specified
maximum width.  If it does not fit, an elision indicator is drawn after the
last character that does fit.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>drawable</parameter>&nbsp;:</term>
<listitem><simpara> Destination drawable.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>font</parameter>&nbsp;:</term>
<listitem><simpara> Font for the text.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>gc</parameter>&nbsp;:</term>
<listitem><simpara> GC to use for drawing.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>x</parameter>&nbsp;:</term>
<listitem><simpara> X insertion point for the string.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>y</parameter>&nbsp;:</term>
<listitem><simpara> Y insertion point for the string's baseline.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>str</parameter>&nbsp;:</term>
<listitem><simpara> the string we're drawing, passed in so we can change the layout if it needs eliding.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>max_width</parameter>&nbsp;:</term>
<listitem><simpara> Maximum width in which the string must fit.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>center</parameter>&nbsp;:</term>
<listitem><simpara> Whether to center the string in the available area if it does fit.
</simpara></listitem></varlistentry>
</variablelist></refsect2>

</refsect1>




</refentry>
