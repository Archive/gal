<refentry id="ETableSpecification">
<refmeta>
<refentrytitle>ETableSpecification</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GAL Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>ETableSpecification</refname><refpurpose>Internal object to keep track of the ETable State.</refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>

<synopsis>



#define     <link linkend="E-TABLE-SPECIFICATION-TYPE:CAPS">E_TABLE_SPECIFICATION_TYPE</link>
typedef     <link linkend="ETableSpecification">ETableSpecification</link>;
<link linkend="ETableSpecification">ETableSpecification</link>* <link linkend="e-table-specification-new">e_table_specification_new</link>
                                            (void);
<link linkend="gboolean">gboolean</link>    <link linkend="e-table-specification-load-from-file">e_table_specification_load_from_file</link>
                                            (<link linkend="ETableSpecification">ETableSpecification</link> *specification,
                                             const <link linkend="char">char</link> *filename);
<link linkend="void">void</link>        <link linkend="e-table-specification-load-from-string">e_table_specification_load_from_string</link>
                                            (<link linkend="ETableSpecification">ETableSpecification</link> *specification,
                                             const <link linkend="char">char</link> *xml);
<link linkend="void">void</link>        <link linkend="e-table-specification-load-from-node">e_table_specification_load_from_node</link>
                                            (<link linkend="ETableSpecification">ETableSpecification</link> *specification,
                                             const <link linkend="xmlNode">xmlNode</link> *node);
<link linkend="void">void</link>        <link linkend="e-table-specification-save-to-file">e_table_specification_save_to_file</link>
                                            (<link linkend="ETableSpecification">ETableSpecification</link> *specification,
                                             const <link linkend="char">char</link> *filename);
<link linkend="char">char</link>*       <link linkend="e-table-specification-save-to-string">e_table_specification_save_to_string</link>
                                            (<link linkend="ETableSpecification">ETableSpecification</link> *specification);
<link linkend="xmlNode">xmlNode</link>*    <link linkend="e-table-specification-save-to-node">e_table_specification_save_to_node</link>
                                            (<link linkend="ETableSpecification">ETableSpecification</link> *specification,
                                             <link linkend="xmlDoc">xmlDoc</link> *doc);

</synopsis>
</refsynopsisdiv>

<refsect1>
<title>Object Hierarchy</title>
<synopsis>

  <link linkend="GtkObject">GtkObject</link>
   +----ETableSpecification
</synopsis>

</refsect1>








<refsect1>
<title>Description</title>
<para>
   This object is used to keep track of the ETableState, and it is
   mostly an internal function.  These are used by the ETable widget
   and its configuration engine (<literal>ETableConfig</literal>).  It is mostly an
   internal object.
</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="E-TABLE-SPECIFICATION-TYPE:CAPS">E_TABLE_SPECIFICATION_TYPE</title>
<indexterm><primary>E_TABLE_SPECIFICATION_TYPE</primary></indexterm><programlisting>#define E_TABLE_SPECIFICATION_TYPE        (e_table_specification_get_type ())
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="ETableSpecification">ETableSpecification</title>
<indexterm><primary>ETableSpecification</primary></indexterm><programlisting>typedef struct {
	GtkObject base;

	ETableColumnSpecification **columns;
	ETableState *state;

	guint no_headers : 1;
	guint click_to_add : 1;
	guint draw_grid : 1;
	ETableCursorMode cursor_mode;
	char *click_to_add_message_;
} ETableSpecification;
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="e-table-specification-new">e_table_specification_new ()</title>
<indexterm><primary>e_table_specification_new</primary></indexterm><programlisting><link linkend="ETableSpecification">ETableSpecification</link>* e_table_specification_new
                                            (void);</programlisting>
<para>
Creates a new <literal>ETableSpecification</literal> object.   This object is used to hold the
information about the rendering information for ETable.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> a newly created <literal>ETableSpecification</literal> object.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="e-table-specification-load-from-file">e_table_specification_load_from_file ()</title>
<indexterm><primary>e_table_specification_load_from_file</primary></indexterm><programlisting><link linkend="gboolean">gboolean</link>    e_table_specification_load_from_file
                                            (<link linkend="ETableSpecification">ETableSpecification</link> *specification,
                                             const <link linkend="char">char</link> *filename);</programlisting>
<para>
This routine modifies <parameter>specification</parameter> to reflect the state described
by the file <parameter>filename</parameter>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>specification</parameter>&nbsp;:</term>
<listitem><simpara> An ETableSpecification that you want to modify
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>filename</parameter>&nbsp;:</term>
<listitem><simpara> a filename that contains an ETableSpecification
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> TRUE on success, FALSE on failure.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="e-table-specification-load-from-string">e_table_specification_load_from_string ()</title>
<indexterm><primary>e_table_specification_load_from_string</primary></indexterm><programlisting><link linkend="void">void</link>        e_table_specification_load_from_string
                                            (<link linkend="ETableSpecification">ETableSpecification</link> *specification,
                                             const <link linkend="char">char</link> *xml);</programlisting>
<para>
This routine modifies <parameter>specification</parameter> to reflect the state described
by <parameter>xml</parameter>.  <parameter>xml</parameter> is typically returned by e_table_specification_save_to_string
or it can be embedded in your source code.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>specification</parameter>&nbsp;:</term>
<listitem><simpara> An ETableSpecification that you want to modify
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>xml</parameter>&nbsp;:</term>
<listitem><simpara> a stringified representation of an ETableSpecification description.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="e-table-specification-load-from-node">e_table_specification_load_from_node ()</title>
<indexterm><primary>e_table_specification_load_from_node</primary></indexterm><programlisting><link linkend="void">void</link>        e_table_specification_load_from_node
                                            (<link linkend="ETableSpecification">ETableSpecification</link> *specification,
                                             const <link linkend="xmlNode">xmlNode</link> *node);</programlisting>
<para>
This routine modifies <parameter>specification</parameter> to reflect the state described
by <parameter>node</parameter>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>specification</parameter>&nbsp;:</term>
<listitem><simpara> An ETableSpecification that you want to modify
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>node</parameter>&nbsp;:</term>
<listitem><simpara> an xmlNode with an XML ETableSpecification description.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="e-table-specification-save-to-file">e_table_specification_save_to_file ()</title>
<indexterm><primary>e_table_specification_save_to_file</primary></indexterm><programlisting><link linkend="void">void</link>        e_table_specification_save_to_file
                                            (<link linkend="ETableSpecification">ETableSpecification</link> *specification,
                                             const <link linkend="char">char</link> *filename);</programlisting>
<para>
This routine stores the <parameter>specification</parameter> into <parameter>filename</parameter>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>specification</parameter>&nbsp;:</term>
<listitem><simpara> An <literal>ETableSpecification</literal> that you want to save
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>filename</parameter>&nbsp;:</term>
<listitem><simpara> a file name to store the specification.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="e-table-specification-save-to-string">e_table_specification_save_to_string ()</title>
<indexterm><primary>e_table_specification_save_to_string</primary></indexterm><programlisting><link linkend="char">char</link>*       e_table_specification_save_to_string
                                            (<link linkend="ETableSpecification">ETableSpecification</link> *specification);</programlisting>
<para>
Saves the state of <parameter>specification</parameter> to a string.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>specification</parameter>&nbsp;:</term>
<listitem><simpara> An <literal>ETableSpecification</literal> that you want to stringify
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> an <link linkend="g-alloc"><function>g_alloc()</function></link> allocated string containing the stringified 
representation of <parameter>specification</parameter>.  This stringified representation
uses XML as a convenience.
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="e-table-specification-save-to-node">e_table_specification_save_to_node ()</title>
<indexterm><primary>e_table_specification_save_to_node</primary></indexterm><programlisting><link linkend="xmlNode">xmlNode</link>*    e_table_specification_save_to_node
                                            (<link linkend="ETableSpecification">ETableSpecification</link> *specification,
                                             <link linkend="xmlDoc">xmlDoc</link> *doc);</programlisting>
<para>
This routine saves the <literal>ETableSpecification</literal> state in the object <parameter>specification</parameter>
into the xmlDoc represented by <parameter>doc</parameter>.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>specification</parameter>&nbsp;:</term>
<listitem><simpara> An ETableSpecification that you want to store.
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>doc</parameter>&nbsp;:</term>
<listitem><simpara> Node where the specification is saved
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> The node that has been attached to <parameter>doc</parameter> with the contents
of the ETableSpecification.
</simpara></listitem></varlistentry>
</variablelist></refsect2>

</refsect1>



<refsect1>
<title>See Also</title>
<para>
  <literal>ETable</literal>, <literal>ETableColumnSpecification</literal>, <literal>ETableConfig</literal>.
</para>
</refsect1>

</refentry>
