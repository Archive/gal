<refentry id="gal-ColorGroup">
<refmeta>
<refentrytitle>ColorGroup</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GAL Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>ColorGroup</refname><refpurpose></refpurpose>
</refnamediv>

<refsynopsisdiv><title>Synopsis</title>

<synopsis>



#define     <link linkend="COLOR-GROUP-TYPE:CAPS">COLOR_GROUP_TYPE</link>
#define     <link linkend="COLOR-GROUP:CAPS">COLOR_GROUP</link>                     (obj)
#define     <link linkend="COLOR-GROUP-CLASS:CAPS">COLOR_GROUP_CLASS</link>               (k)
#define     <link linkend="IS-COLOR-GROUP:CAPS">IS_COLOR_GROUP</link>                  (obj)
<link linkend="GtkType">GtkType</link>     <link linkend="color-group-get-type">color_group_get_type</link>            (void);
<link linkend="GtkObject">GtkObject</link>*  <link linkend="color-group-new-named">color_group_new_named</link>           (const <link linkend="gchar">gchar</link> *name);
<link linkend="GtkObject">GtkObject</link>*  <link linkend="color-group-from-name">color_group_from_name</link>           (const <link linkend="gchar">gchar</link> *name);
<link linkend="void">void</link>        <link linkend="color-group-set-history-size">color_group_set_history_size</link>    (<link linkend="ColorGroup">ColorGroup</link> *cg,
                                             <link linkend="gint">gint</link> size);
<link linkend="gint">gint</link>        <link linkend="color-group-get-history-size">color_group_get_history_size</link>    (<link linkend="ColorGroup">ColorGroup</link> *cg);
<link linkend="GdkColor">GdkColor</link>*   <link linkend="color-group-get-current-color">color_group_get_current_color</link>   (<link linkend="ColorGroup">ColorGroup</link> *cg);
<link linkend="void">void</link>        <link linkend="color-group-set-current-color">color_group_set_current_color</link>   (<link linkend="ColorGroup">ColorGroup</link> *cg,
                                             <link linkend="GdkColor">GdkColor</link> *color);
<link linkend="GdkColor">GdkColor</link>*   <link linkend="color-group-most-recent-color">color_group_most_recent_color</link>   (<link linkend="ColorGroup">ColorGroup</link> *cg);
<link linkend="GdkColor">GdkColor</link>*   <link linkend="color-group-oldest-color">color_group_oldest_color</link>        (<link linkend="ColorGroup">ColorGroup</link> *cg);
<link linkend="GdkColor">GdkColor</link>*   <link linkend="color-group-next-color">color_group_next_color</link>          (<link linkend="ColorGroup">ColorGroup</link> *cg);
<link linkend="GdkColor">GdkColor</link>*   <link linkend="color-group-previous-color">color_group_previous_color</link>      (<link linkend="ColorGroup">ColorGroup</link> *cg);
<link linkend="void">void</link>        <link linkend="color-group-add-color">color_group_add_color</link>           (<link linkend="ColorGroup">ColorGroup</link> *cg,
                                             <link linkend="GdkColor">GdkColor</link> *color,
                                             <link linkend="gboolean">gboolean</link> custom_color);
</synopsis>
</refsynopsisdiv>









<refsect1>
<title>Description</title>
<para>

</para>
</refsect1>

<refsect1>
<title>Details</title>
<refsect2>
<title><anchor id="COLOR-GROUP-TYPE:CAPS">COLOR_GROUP_TYPE</title>
<indexterm><primary>COLOR_GROUP_TYPE</primary></indexterm><programlisting>#define COLOR_GROUP_TYPE     (color_group_get_type ())
</programlisting>
<para>

</para></refsect2>
<refsect2>
<title><anchor id="COLOR-GROUP:CAPS">COLOR_GROUP()</title>
<indexterm><primary>COLOR_GROUP</primary></indexterm><programlisting>#define COLOR_GROUP(obj)     (GTK_CHECK_CAST((obj), COLOR_GROUP_TYPE, ColorGroup))
</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>obj</parameter>&nbsp;:</term>
<listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="COLOR-GROUP-CLASS:CAPS">COLOR_GROUP_CLASS()</title>
<indexterm><primary>COLOR_GROUP_CLASS</primary></indexterm><programlisting>#define COLOR_GROUP_CLASS(k) (GTK_CHECK_CLASS_CAST(k), COLOR_GROUP_TYPE)
</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>k</parameter>&nbsp;:</term>
<listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="IS-COLOR-GROUP:CAPS">IS_COLOR_GROUP()</title>
<indexterm><primary>IS_COLOR_GROUP</primary></indexterm><programlisting>#define IS_COLOR_GROUP(obj)  (GTK_CHECK_TYPE((obj), COLOR_GROUP_TYPE))
</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>obj</parameter>&nbsp;:</term>
<listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="color-group-get-type">color_group_get_type ()</title>
<indexterm><primary>color_group_get_type</primary></indexterm><programlisting><link linkend="GtkType">GtkType</link>     color_group_get_type            (void);</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="color-group-new-named">color_group_new_named ()</title>
<indexterm><primary>color_group_new_named</primary></indexterm><programlisting><link linkend="GtkObject">GtkObject</link>*  color_group_new_named           (const <link linkend="gchar">gchar</link> *name);</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>name</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="color-group-from-name">color_group_from_name ()</title>
<indexterm><primary>color_group_from_name</primary></indexterm><programlisting><link linkend="GtkObject">GtkObject</link>*  color_group_from_name           (const <link linkend="gchar">gchar</link> *name);</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>name</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="color-group-set-history-size">color_group_set_history_size ()</title>
<indexterm><primary>color_group_set_history_size</primary></indexterm><programlisting><link linkend="void">void</link>        color_group_set_history_size    (<link linkend="ColorGroup">ColorGroup</link> *cg,
                                             <link linkend="gint">gint</link> size);</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>cg</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>size</parameter>&nbsp;:</term>
<listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="color-group-get-history-size">color_group_get_history_size ()</title>
<indexterm><primary>color_group_get_history_size</primary></indexterm><programlisting><link linkend="gint">gint</link>        color_group_get_history_size    (<link linkend="ColorGroup">ColorGroup</link> *cg);</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>cg</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="color-group-get-current-color">color_group_get_current_color ()</title>
<indexterm><primary>color_group_get_current_color</primary></indexterm><programlisting><link linkend="GdkColor">GdkColor</link>*   color_group_get_current_color   (<link linkend="ColorGroup">ColorGroup</link> *cg);</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>cg</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="color-group-set-current-color">color_group_set_current_color ()</title>
<indexterm><primary>color_group_set_current_color</primary></indexterm><programlisting><link linkend="void">void</link>        color_group_set_current_color   (<link linkend="ColorGroup">ColorGroup</link> *cg,
                                             <link linkend="GdkColor">GdkColor</link> *color);</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>cg</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>color</parameter>&nbsp;:</term>
<listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="color-group-most-recent-color">color_group_most_recent_color ()</title>
<indexterm><primary>color_group_most_recent_color</primary></indexterm><programlisting><link linkend="GdkColor">GdkColor</link>*   color_group_most_recent_color   (<link linkend="ColorGroup">ColorGroup</link> *cg);</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>cg</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="color-group-oldest-color">color_group_oldest_color ()</title>
<indexterm><primary>color_group_oldest_color</primary></indexterm><programlisting><link linkend="GdkColor">GdkColor</link>*   color_group_oldest_color        (<link linkend="ColorGroup">ColorGroup</link> *cg);</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>cg</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="color-group-next-color">color_group_next_color ()</title>
<indexterm><primary>color_group_next_color</primary></indexterm><programlisting><link linkend="GdkColor">GdkColor</link>*   color_group_next_color          (<link linkend="ColorGroup">ColorGroup</link> *cg);</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>cg</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="color-group-previous-color">color_group_previous_color ()</title>
<indexterm><primary>color_group_previous_color</primary></indexterm><programlisting><link linkend="GdkColor">GdkColor</link>*   color_group_previous_color      (<link linkend="ColorGroup">ColorGroup</link> *cg);</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>cg</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2>
<title><anchor id="color-group-add-color">color_group_add_color ()</title>
<indexterm><primary>color_group_add_color</primary></indexterm><programlisting><link linkend="void">void</link>        color_group_add_color           (<link linkend="ColorGroup">ColorGroup</link> *cg,
                                             <link linkend="GdkColor">GdkColor</link> *color,
                                             <link linkend="gboolean">gboolean</link> custom_color);</programlisting>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>cg</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>color</parameter>&nbsp;:</term>
<listitem><simpara>
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>custom_color</parameter>&nbsp;:</term>
<listitem><simpara>


</simpara></listitem></varlistentry>
</variablelist></refsect2>

</refsect1>




</refentry>
