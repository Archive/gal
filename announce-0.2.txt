Hello lovers of code reuse, a new gal is available: Gal 0.2, code name
"Marie Curie", is out.

Marie Curie was a nobel prize winner and early researcher in nuclear
radioactivity.  You can find more information about her at
http://www.nobel.se/chemistry/laureates/1911/marie-curie-bio.html.

Gal is the Gnome Application Library, a collection of widgets and
other helper functions from Evolution and Gnumeric.

* Availability

You can get the Gal 0.2 tarball here:

ftp://ftp.gnome.org/pub/GNOME/unstable/sources/gal

Gal 0.2 also requires GNOME Print (0.24) and libunicode (0.4.gnome).

ftp://ftp.gnome.org/pub/GNOME/stable/sources/gnome-print
ftp://ftp.gnome.org/pub/GNOME/unstable/sources/libunicode

Helix GNOME users will also be able to install the packages by the
usual means, by using Helix Update (or apt-get on Debian).  Packages
for Debian, Red Hat 6.x and 7.0, Caldera 2.4, Mandrake 6.1, 7.0, and
7.1, Suse 6.3, 6.4 and 7.0, TurboLinux 6, and LinuxPPC 2000 will be
available within the next week.

Have fun,
  Chris
