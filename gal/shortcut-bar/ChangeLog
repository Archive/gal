2004-02-12  Rodney Dawes  <dobey@ximian.com>

	* e-vscrolled-bar.c (e_vscrolled_bar_init): Change the
	shadow type for the arrows here, so that themes that do handle the
	use of shadow types with arrows, draw things correctly, instead of
	following what the old default gtk+ did in 1.x

2003-06-16  Mike Kestner  <mkestner@ximian.com>

	* e-vscrolled-bar.c (e_vscrolled_bar_init): remove padding 
	from arrows, it made the buttons large and offcenter [42861]
	Also remove redundant null checks that are performed by _IS_.

2003-05-16  Mike Kestner  <mkestner@ximian.com>

	* e-group-bar.c (e_group_bar_forall): iterate over the group
	children in reverse order like the group buttons. Fixes 42395.

2003-05-07  Mike Kestner  <mkestner@ximian.com>

	* e-icon-bar.c (e_icon_bar_drag_motion): call gdk_drag_status
	and return FALSE so that the shortcut-bar signal handlers run.
	* e-shortcut-bar.c (e_shortcut_bar_add_group): connect_after
	on the drag_motion signal.

2003-04-16  Mike Kestner  <mkestner@ximian.com>

	* e-icon-bar.c (e_i_b_add_item): get the style from the widget.
	this reverts a part of yesterday's patch that was causing an
	uninitialized data crash.

2003-04-15  Mike Kestner  <mkestner@ximian.com>

	* e-icon-bar.c (e_i_b_recalc_item_positions): s/GdkFont/PangoFont
	(e_i_b_add_item): ditto
	Patch from Chris Toshok <toshok@ximiam.com>

2003-04-03  Mike Kestner  <mkestner@ximian.com>

	* e-icon-bar.c (e_icon_bar_size_allocate): save/restore the
	adjustment->value around gnome_canvas_set_scroll_region so that
	it doesn't arbitrarily scroll on us. bug #40753.

2003-03-31  Mike Kestner  <mkestner@ximian.com>

	* e-vscrolled-bar.c (e_vscrolled_bar_class_init): hookup expose
	(e_vscrolled_bar_init): stuff the buttons in eboxen so raise works
	(e_vscrolled_bar_dispose): unparent the eboxen
	(e_vscrolled_bar_map): map the eboxen
	(e_vscrolled_bar_unmap): unmap the eboxen
	(e_vscrolled_bar_size_request): sizereq the eboxen
	(e_vscrolled_bar_size_allocate): alloc the eboxen, offset from 
	alloc position.
	(e_vscrolled_bar_expose): roughed in by Ettore. A few tweaks by me.
	(e_vscrolled_bar_forall): don't pass to internals
	(e_vscrolled_bar_adjustment_changed): map, unmap, and raise eboxen.
	Fix for bug #38855.
	
2003-02-24  Rodney Dawes  <dobey@ximian.com>

	* e-group-bar.c:
	* e-icon-bar-bg-item.c:
	* e-icon-bar.c:
	* e-shortcut-bar.c: Make shortcut-bar themeable with bg[ACTIVE]
	
2003-02-19  Dan Winship  <danw@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_group_button_press): Fix the
	prototype and return FALSE so that whether or not the group
	expands doesn't depend on stack garbage

2003-01-27  Jody Goldberg <jody@gnome.org>

	* Release 1.99.1

2003-01-14  Chris Toshok  <toshok@ximian.com>

	* e-group-bar.c (e_group_bar_size_allocate): raise the
	button/child windows so things stay proper looking.

2002-12-08  Chris Toshok  <toshok@ximian.com>

	* e-icon-bar.c (e_icon_bar_style_set): don't bother setting the
	font_gdk on the e-text items, as its ignored.
	(e_icon_bar_add_item): same.

2002-11-16  Chris Toshok  <toshok@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_new): bah, didn't mean to
	change the TYPE.

	* e-shortcut-bar.h (E_SHORTCUT_BAR_TYPE): same.

2002-11-16  Chris Toshok  <toshok@ximian.com>

	* e-group-bar.[ch]: lotsa GObject work.

	* e-icon-bar.[ch]: same.

	* e-icon-bar-bg-item.[ch]: same.

	* e-shortcut-bar.[ch]: same.

	* e-shortcut-model.[ch]: same.

	* e-vscrolled-bar.[ch]: same.

2002-11-14  Ettore Perazzoli  <ettore@ximian.com>

	* e-group-bar.c (e_group_bar_expose): Removed.

2002-11-14  Ettore Perazzoli  <ettore@ximian.com>

	* e-group-bar.c (e_group_bar_stop_all_animation): Protect from
	group_bar->children being NULL.
	(e_group_bar_forall): Likewise.

	* e-shortcut-bar.c (e_shortcut_bar_dispose): New.
	(e_shortcut_bar_finalize): New.
	(e_shortcut_bar_destroy): Removed.
	(e_shortcut_bar_class_init): Install GObject::dispose/finalize
	handlers instead of GObject::destroy.
	(e_shortcut_bar_disconnect_model): New arg model_destroyed; if
	TRUE, then do not disconnect signals from the model.  Also, if !
	model_destroyed, g_object_weak_unref() the model.
	(e_shortcut_bar_on_model_destroyed): Changed prototype to match
	that of a GWeakNotify.
	(e_shortcut_bar_set_model): Use g_object_weak_ref() instead of
	connecting to the "destroy" signal.

2002-10-26  Jody Goldberg <jody@gnome.org>

	* Release 2.0.0.7

2002-09-13  Jody Goldberg <jody@gnome.org>

	* Release 2.0.0.6

2002-08-27  Ettore Perazzoli  <ettore@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_on_drag_motion): If the
	"shortcut_drag_motion" signal handler returns FALSE, handle the
	condition properly through gdk_drag_status() and return TRUE
	[instead of FALSE], otherwise we don't get any more drag_motion
	events after the first one.  [#23982]

2002-08-06  Jody Goldberg <jody@gnome.org>

	* Release 2.0.0.5

2002-07-03  Ettore Perazzoli  <ettore@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_on_drag_motion): Removed some
	debugging messages.
	(e_shortcut_bar_on_drag_drop): Look for the E-SHORTCUT type in the
	list targets, and request it if present.
	(e_shortcut_bar_on_drag_data_received): Handle the E-SHORTCUT type
	only if the drop is between two folders.  Also use before_item
	instead of position in that case, for the "shortcut_dropped"
	signal, so that the drop actually works.  [#26265]

2002-05-29  Jody Goldberg <jody@gnome.org>

	* Release 2.0.0.4

2002-05-28  Christopher James Lahey  <clahey@ximian.com>

	* Merged up to GAL_2_ANCHOR_MERGE2.

2002-04-22  Christopher James Lahey  <clahey@ximian.com>

	* e-icon-bar.c (e_icon_bar_drag_motion): Removed an unused
	variable.

2002-04-17  Ettore Perazzoli  <ettore@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_class_init): Add the signals
	here.
	(e_shortcut_bar_on_drag_data_received): Emit
	"shortcut_drag_data_received" here.  If the signal handler returns
	%FALSE, do a gtk_drag_finish() reporting failure.
	(e_shortcut_bar_marshal_BOOL__POINTER_POINTER_INT_INT_INT): New
	marshaller.
	(e_shortcut_bar_marshal_BOOL__POINTER_POINTER_POINTER_INT_INT_INT):
	Another mashaller.  I love you GTK.
	(e_shortcut_bar_on_drag_motion): New callback for the
	::drag_motion signal.
	(e_shortcut_bar_on_drag_drop): Always invoke gtk_drag_get_data()
	no matter what the type is -- we'll handle the type in
	::data_received.
	(e_shortcut_bar_on_drag_data_received): Get the position correctly
	by using e_icon_bar_find_item_at_position().

	* e-shortcut-bar.h: New signals "shortcut_drag_motion" and
	"shortcut_drag_data_received".

2002-04-16  Ettore Perazzoli  <ettore@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_on_drag_drop): New.
	(e_shortcut_bar_add_group): Connect.

	* e-icon-bar.c (e_icon_bar_init): Set the icon bar up as a drag
	target.
	(e_icon_bar_drag_motion): Only return TRUE if one of the possible
	targets is E-SHORTCUT, and the user is moving the pointer between
	two items.

	* e-group-bar.c (e_group_bar_add_group): Set the group button as a
	dnd destination here.
	(e_group_bar_on_button_drag_motion): Always return FALSE here as
	we never really allow any dropping of things onto the group
	header/button anyways.

	* e-shortcut-bar.c (e_shortcut_bar_add_group): Don't set the icon
	bar or its button as a destination.

2002-03-26  Chris Toshok  <toshok@ximian.com>

	* Makefile.am: remove comment about adding .lo's to the parent
	directory's Makefile.am.

2002-04-22  Christopher James Lahey  <clahey@ximian.com>

	* e-icon-bar.c (e_icon_bar_drag_motion): Removed an unused
	variable.

2002-04-17  Ettore Perazzoli  <ettore@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_class_init): Add the signals
	here.
	(e_shortcut_bar_on_drag_data_received): Emit
	"shortcut_drag_data_received" here.  If the signal handler returns
	%FALSE, do a gtk_drag_finish() reporting failure.
	(e_shortcut_bar_marshal_BOOL__POINTER_POINTER_INT_INT_INT): New
	marshaller.
	(e_shortcut_bar_marshal_BOOL__POINTER_POINTER_POINTER_INT_INT_INT):
	Another mashaller.  I love you GTK.
	(e_shortcut_bar_on_drag_motion): New callback for the
	::drag_motion signal.
	(e_shortcut_bar_on_drag_drop): Always invoke gtk_drag_get_data()
	no matter what the type is -- we'll handle the type in
	::data_received.
	(e_shortcut_bar_on_drag_data_received): Get the position correctly
	by using e_icon_bar_find_item_at_position().

	* e-shortcut-bar.h: New signals "shortcut_drag_motion" and
	"shortcut_drag_data_received".

2002-04-16  Ettore Perazzoli  <ettore@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_on_drag_drop): New.
	(e_shortcut_bar_add_group): Connect.

	* e-icon-bar.c (e_icon_bar_init): Set the icon bar up as a drag
	target.
	(e_icon_bar_drag_motion): Only return TRUE if one of the possible
	targets is E-SHORTCUT, and the user is moving the pointer between
	two items.

	* e-group-bar.c (e_group_bar_add_group): Set the group button as a
	dnd destination here.
	(e_group_bar_on_button_drag_motion): Always return FALSE here as
	we never really allow any dropping of things onto the group
	header/button anyways.

	* e-shortcut-bar.c (e_shortcut_bar_add_group): Don't set the icon
	bar or its button as a destination.

2002-03-26  Jody Goldberg <jody@gnome.org>

	* Release 2.0.0.2

2002-03-26  Chris Toshok  <toshok@ximian.com>

	* Makefile.am: remove comment about adding .lo's to the parent
	directory's Makefile.am.

2002-03-10  Jody Goldberg <jody@gnome.org>

	* Release 2.0.1

2002-02-17  Kjartan Maraas  <kmaraas@gnome.org>

	* e-shortcut-bar.c: Remove calls to deprecated functions
	gtk_widget_push|pop_visual().
	
2001-10-24  Christopher James Lahey  <clahey@ximian.com>

	* e-group-bar.c, e-group-bar.h, e-icon-bar-bg-item.c,
	e-icon-bar-bg-item.h, e-icon-bar.c, e-icon-bar.h,
	e-shortcut-bar.c, e-shortcut-bar.h, e-shortcut-model.c,
	e-shortcut-model.h, e-vscrolled-bar.c, e-vscrolled-bar.h: Changed
	the license announcement at the top of these files.

2001-10-24  Christopher James Lahey  <clahey@ximian.com>

	* Makefile.am (INCLUDES): Added $(ICONV_CFLAGS).

2001-10-23  Christopher James Lahey  <clahey@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_add_group): Turn on
	"draw_button" argument to EEntry.  Fixes Ximian bug #1273.

2001-10-04  Jon Trowbridge  <trow@ximian.com>

	* e-icon-bar.c (e_icon_bar_add_item): Don't call gdk_pixbuf_ref
	if image is NULL.

	* e-shortcut-model.c (e_shortcut_model_real_get_item_info): Check
	to make sure we don't call gdk_pixbuf_ref on NULL, which causes a
	warning.

2001-10-24  Christopher James Lahey  <clahey@ximian.com>

	* e-group-bar.c, e-group-bar.h, e-icon-bar-bg-item.c,
	e-icon-bar-bg-item.h, e-icon-bar.c, e-icon-bar.h,
	e-shortcut-bar.c, e-shortcut-bar.h, e-shortcut-model.c,
	e-shortcut-model.h, e-vscrolled-bar.c, e-vscrolled-bar.h: Changed
	the license announcement at the top of these files.

2001-10-24  Christopher James Lahey  <clahey@ximian.com>

	* Makefile.am (INCLUDES): Added $(ICONV_CFLAGS).

2001-10-23  Christopher James Lahey  <clahey@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_add_group): Turn on
	"draw_button" argument to EEntry.  Fixes Ximian bug #1273.

2001-10-04  Jon Trowbridge  <trow@ximian.com>

	* e-icon-bar.c (e_icon_bar_add_item): Don't call gdk_pixbuf_ref
	if image is NULL.

	* e-shortcut-model.c (e_shortcut_model_real_get_item_info): Check
	to make sure we don't call gdk_pixbuf_ref on NULL, which causes a
	warning.

2001-09-14  Ettore Perazzoli  <ettore@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_group_button_press): Callback
	for button_press_event on the group buttons.  Emit "item_selected"
	for the corresponding group [i.e. with an @item value of -1].
	(e_shortcut_bar_add_group): Connect the signal to the newly
	created button.

2001-08-24  Damon Chaplin  <damon@ximian.com>

	* e-group-bar.c (e_group_bar_get_group_button): new function to get
	a group's button.

	* e-vscrolled-bar.c (e_vscrolled_bar_scroll): patch from
	kiko@async.com.br (Christian Reis) and clahey to make sure we only
	call GDK_THREADS_ENTER/LEAVE() from a timeout, and not from ordinary
	code.

2001-08-21  Damon Chaplin  <damon@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_set_canvas_style): changed the
	background color slightly to match the EGrayBar in the Evolution shell.

2001-08-09  Lutz M�ller  <urc8@rz.uni-karlsruhe.de>
	
	* e-shortcut-bar.c: (e_shortcut_bar_on_drag_data_received): Check if 
	info == TARGET_SHORTCUT, if not, simply ignore the drag data.

2001-08-07  Ettore Perazzoli  <ettore@ximian.com>

	* e-shortcut-model.c: Add member `image' to EShortcutModelItem.
	(e_shortcut_model_marshal2): Updated to marshal a pointer at the
	end too.
	(e_shortcut_model_real_get_item_info): New arg @image.  Return the
	pixbuf there.
	(e_shortcut_model_add_item): New arg @item_image; use it.
	(e_shortcut_model_real_add_item): Likewise.
	(e_shortcut_model_update_item): Likewise.
	(e_shortcut_model_real_update_item): Likewise.

	* e-shortcut-model.h: New arg @image added to signals
	`item_added', `item_updated', `get_item_info'.

	* e-shortcut-bar.c (e_shortcut_bar_set_icon_callback): Removed.
	(e_shortcut_bar_get_image_from_url): Removed.
	(e_shortcut_bar_add_item): New arg @image.  Use that as the icon
	instead of using `e_shortcut_bar_get_image_from_url'.
	(e_shortcut_bar_update_item): Likewise.
	(e_shortcut_bar_load_image): Removed.
	(e_shortcut_bar_on_item_updated): New arg @image; use it.
	(e_shortcut_bar_on_item_added): New arg @image; use it.

	* e-shortcut-bar.h (struct _EShortcutBar): Remove members
	`icon_callback', `icon_callback_data'.

2001-07-30  Damon Chaplin  <damon@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_on_drag_data_get): fixed typo.

2001-07-28  Damon Chaplin  <damon@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_on_drag_data_get): check that
	dragged_name and dragged_url are set before setting the selection
	data. Occasionally we get called with them set to NULL, which is a
	bit strange. This may fix bug #4524.

2001-07-25  Jason Leach  <jleach@ximian.com>

	* e-icon-bar.c (e_icon_bar_set_item_image): Fix a problem of
	updating a shortcut item losing the alpha transparency for the
	icon.

2001-06-04  Jason Leach  <jleach@ximian.com>

	(Implementing shortcut item updating, for bug #1299)
	
	* e-shortcut-model.c (e_shortcut_model_class_init): New signal:
	"item_updated", callbacks in same style as add.
	(e_shortcut_model_real_update_item): New function.
	(e_shortcut_model_update_item): New function, this function is
	what we will call in Shell.

	* e-shortcut-bar.c (e_shortcut_bar_set_model): Connect the new signal
	here.
	(e_shortcut_bar_on_item_updated): Callback processing here.

2001-06-04  Jason Leach  <jleach@ximian.com>

	* e-icon-bar.c (e_icon_bar_set_item_text): Made @text const.

2001-05-14  Damon Chaplin  <damon@ximian.com>

	* e-icon-bar.c (e_icon_bar_recalc_item_positions): added workaround
	for what seems to be a bug in EText - it is returning a height which is
	too big in the gnome_canvas_item_get_bounds() call. At least DnD should
	be OK with small icons now.

2001-04-04  Kjartan Maraas  <kmaraas@gnome.org>

	* e-icon-bar.c: Fix headers.
	* e-shortcut-bar.c: Same here.

2001-04-04  Christopher James Lahey  <clahey@ximian.com>

	* e-icon-bar.c: Fixed headers here.

2001-03-15  Dan Winship  <danw@ximian.com>

	* e-icon-bar.c (e_icon_bar_start_editing_item): Update arguments
	to e_canvas_item_grab_focus

2001-02-25  Damon Chaplin  <damon@ximian.com>

	* e-shortcut-bar.h (struct _EShortcutBar): added enable_drags boolean.

	* e-icon-bar.h (struct _EIconBar): added enable_drags boolean.

	* e-shortcut-bar.c (e_shortcut_bar_set_enable_drags):
	* e-icon-bar.c (e_icon_bar_set_enable_drags): new functions so you
	can disable dragging. Needed to do some pointer grabbing as well.

2001-01-19  JP Rosevear  <jpr@ximian.com>

	* e-shortcut-bar.c (e_shortcut_bar_new): Visual/colormap love

2001-01-17  Miguel de Icaza  <miguel@gnu.org>

	* e-group-bar.c: Use e-i18n.h

2000-12-27  Christopher James Lahey  <clahey@helixcode.com>

	* Makefile.am: Rewrote this to work like most of the gal
	directories do.  Moved test-shortcut-bar to the gal/tests
	directory.

	* e-shortcut-bar.c: Changed this to use an EEntry with background
	and border drawing turned off instead of an EClippedLabel.

	* test-shortcut-bar.c: Moved this to gal/tests.

2000-12-25  Miguel de Icaza  <miguel@helixcode.com>

	* e-icon-bar.c (e_icon_bar_add_item): Ditto.

2000-09-30  Damon Chaplin  <damon@helixcode.com>

	* test-shortcut-bar.c: updated to use new EShortcutBar signals for DnD.

	* e-group-bar.c (e_group_bar_add_group): show the new windows if the
	EGroupBar is realized, handle the position argument properly, and
	fixed the window z-order code.

	* e-shortcut-bar.c (e_shortcut_bar_add_group): pass group_num to the
	e_group_bar_add_group() function.

	* e-icon-bar-bg-item.c (e_icon_bar_bg_item_draw): fixed SEGV when
	trying to drag into a group with no items.

2000-09-28  Ettore Perazzoli  <ettore@helixcode.com>

	* e-shortcut-bar.c (e_shortcut_bar_destroy): Disconnect the model
	before freeing the group array.

2000-09-26  Ettore Perazzoli  <ettore@helixcode.com>

	* e-shortcut-bar.c (e_shortcut_bar_on_drag_data_delete): Emit the
	signal correctly.

2000-09-25  Ettore Perazzoli  <ettore@helixcode.com>

	* e-shortcut-bar.c (e_shortcut_bar_destroy): Call
	`e_shortcut_bar_disconnect_model()'.
	(e_shortcut_bar_on_drag_data_received): Don't add the item to the
	model here; the view is only supposed to act as a view.  Rather,
	emit the "dropped_shortcut" signal appropriately.
	(e_shortcut_bar_on_drag_data_delete): Likewise, don't remove the
	item from the model and emit "shortcut_dragged".
	(e_shortcut_bar_class_init): Install the "shortcut_dropped" and
	"shortcut_dragged" signals.

	* e-shortcut-bar.h: New signals "shortcut_dropped",
	"shortcut_dragged".  "selected_item" changed to "item_selected".

2000-09-18  Christopher James Lahey  <clahey@helixcode.com>

	* Makefile.am: Added $(EXTRA_GNOME_CFLAGS) and
	$(EXTRA_GNOME_LIBS).  Removed unneeded libraries.

	* e-icon-bar.c, e-icon-bar.h, e-shortcut-bar.c,
	e-shortcut-model.c, test-shortcut-bar.c: Fixed the #include lines
	to deal properly with gal.

2000-08-16  Larry Ewing  <lewing@helixcode.com>

	* e-icon-bar.c (e_icon_bar_init): remove COLOR_TEXT references.

2000-08-16  Larry Ewing  <lewing@helixcode.com>

	* e-icon-bar.h (enum): get rid of COLOR_TEXT.
	(struct _EIconBarItem): add pixbuf member to hold unmodified
	pixbuf.

	* e-icon-bar.c (e_icon_bar_style_set): update the image and the
	colors based on the new style.
	(e_icon_bar_add_item): store the full alpha pixbuf so that we can
	update the background color if the style changes.  Use the style
	colors where appropriate.
	(e_icon_bar_on_editing_stopped): revert to style colors.
	(e_icon_bar_on_editing_started): use style fg instead of
	COLOR_TEXT.
	
	* e-shortcut-bar.c (e_shortcut_bar_add_group): add an #ifndef
	E_USE_STYLES around the call to e_shortcut_bar_set_canvas_style to
	allow people to test out the style stuff that is in progress.
	

2000-07-16  Damon Chaplin  <damon@helixcode.com>

	* e-shortcut-model.[hc]: new files implementing a simple model for
	the shortcuts, so we can have multiple views.

	* Makefile.am (libshortcut_bar_a_SOURCES): added e-shortcut-model.[hc]

	* e-shortcut-bar.h: updated to use the model.

	* e-icon-bar.c (e_icon_bar_item_pressed): set mouse_over_item_num as
	well as pressed_item_num to fix bug.

2000-07-10  Ettore Perazzoli  <ettore@helixcode.com>

	* e-group-bar.c (e_group_bar_add): Set `group_bar'.

2000-06-19  Damon Chaplin  <damon@helixcode.com>

	* e-icon-bar-bg-item.c:
	* e-icon-bar.c:
	* e-icon-bar.h:
	* e-vscrolled-bar.c: added support for scroll-wheels.

2000-06-02  Christopher James Lahey  <clahey@helixcode.com>

	* e-icon-bar.c: Free all the item data.

2000-06-01  Ettore Perazzoli  <ettore@helixcode.com>

	* e-shortcut-bar.c (e_shortcut_bar_get_view_type): New function.

2000-05-25  Ettore Perazzoli  <ettore@helixcode.com>

	* e-shortcut-bar.c (e_shortcut_bar_load_image): Leak plug: free
	pathname returned from `gnome_pixmap_file()'.

	* Makefile.am: Add `-I$(top_srcdir)'.

2000-05-24  Christopher James Lahey  <clahey@helixcode.com>

	* Makefile.am: Added libepaned.a.

	* test-shortcut-bar.c: Switched from GtkPaned to EPaned.

2000-05-18  Damon Chaplin  <damon@helixcode.com>

	* e-shortcut-bar.[hc]: new signals "added_item", "removed_item",
	"added_group", "removed_group". Note that the removed signals are
	emitted just before the item/group is actually removed.

	* test-shortcut-bar.c: updated to test the new signals, and ref'ed
	the pixbufs in the icon callback.

2000-05-16  Ettore Perazzoli  <ettore@helixcode.com>

	* e-clipped-label.c: Moved to $(top_srcdir)/widgets/misc.
	* e-clipped-label.h: Likewise.
	* Makefile.am: Updated accordingly.

2000-05-07  Larry Ewing  <lewing@helixcode.com>

	* e-icon-bar.h (struct _EIconBarItem): remove the no longer needed
	flatened_alpha member.

	* e-icon-bar.c (flatten_alpha): using ~rgb can cause visual
	artifacts.
	(e_icon_bar_add_item): fixup refcounting of the flattend pixbuf,
	the canvas item should hold the reference not us. Stop using
	item.flattened_alpha since it no longer exists.
	(e_icon_bar_remove_item): remove the unref since destroying the
	item will take care of that for us.

	* e-shortcut-bar.c (e_shortcut_bar_add_item): make sure to unref
	the image we get back, the icon bar will hold any references it
	needs.
	(e_shortcut_bar_get_image_from_url): ref the default image each
	time we return it so that references are counted correctly.

2000-05-04  Ettore Perazzoli  <ettore@helixcode.com>

	* e-shortcut-bar.h (E_TYPE_SHORTCUT_BAR): New #define.

2000-05-04  Ettore Perazzoli  <ettore@helixcode.com>

	* e-icon-bar-bg-item.c (e_icon_bar_bg_item_draw): Initialize
	`shadow' to placate compiler.

	* e-icon-bar.c (e_icon_bar_add_item): @text made const-safe.

	* e-shortcut-bar.c (e_shortcut_bar_add_group): @group_name made
	const-safe.
	(e_shortcut_bar_add_item): @item_url and @item_name made
	const-safe.
	(e_shortcut_bar_get_image_from_url): @item_url made const-safe.
	(e_shortcut_bar_load_image): @filename made const-safe.

2000-05-04  Ettore Perazzoli  <ettore@helixcode.com>

	* test-shortcut-bar.c (icon_callback): Adapted to the new
	`EShortcutBarIconCallback' typedef: get a @data parameter and get
	a const-safe @url.

	* e-shortcut-bar.c (e_shortcut_bar_set_icon_callback): New param
	@data.  Set `icon_callback_data' to it.
	(e_shortcut_bar_get_image_from_url): Pass the callback data to the
	callback.

	* e-shortcut-bar.h: Changed `EShortcutBarIconCallback' to get a
	const-safe @url parameter and a new @data parameter as a closure.
	New member `data' in `EShortcutBar'.

2000-05-02  Matt Loper  <matt@helixcode.com>

	* Makefile.am: set G_LOG_DOMAIN.

2000-04-16  Damon Chaplin  <damon@helixcode.com>

	* test-shortcut-bar.c: added buttons to test moving/reordering groups.

	* e-shortcut-bar.c (e_shortcut_bar_on_drag_end): new function to free
	the dragged name & URL when the drag is finished.

	* e-group-bar.c (e_group_bar_get_increment): fixed bug when distance
	was 0. Should just return 0.
	(e_group_bar_set_current_group_num): added animate option and finished.
	(e_group_bar_unmap): called e_group_bar_stop_all_animation().
	(e_group_bar_reorder_group): finished.

	* e-icon-bar-text-item.[hc]: removed, since we now use EText.
	

2000-04-15  Miguel de Icaza  <miguel@gnu.org>

	* e-group-bar.c (e_group_bar_realize): Do not use the parent
	thingie, because it might be like anything.  And anything can be
	like any visual, and we dont like want any visual here.

	* e-shortcut-bar.c
	(e_shortcut_bar_add_group): Apply some loving action when it comes
	to visuals and colormaps.  Yes, we got one of those machines
	--again--.  Sigh.  This is what you get when you have been away
	from the Indy.  The whole thing is breaking left and right.

	Hopefully, Gtk 1.4 fixes all of this, by always running in the
	slowest visual it can find.

2000-04-11  Larry Ewing  <lewing@helixcode.com>
	
	* e-icon-bar.c (flatten_alpha): update for
	new gdk-pixbuf.

2000-03-31  Damon Chaplin  <damon@helixcode.com>

	* e-shortcut-bar.[hc]: added support for a
	callback function to set the icons.

	* test-shortcut-bar.c: updated to use the
	callback function.

2000-03-30  Dan Winship  <danw@helixcode.com>
	
	* e-shortcut-bar.c: Sort of use the new
	pixmaps. This code needs to be restructured, but Damon is
	rumored to already be doing it.

2000-03-27  Miguel de Icaza  <miguel@gnu.org>

	* e-icon-bar.c (flatten_alpha): New function,
	used to flatten the images we get and composite them with the
	background. 
	(e_icon_bar_add_item): One happy tigert comming.

2000-03-21  Christopher James Lahey  <clahey@helixcode.com>

	* e-icon-bar.c: Updated to fix a few bugs.

2000-02-28  NotZed  <NotZed@HelixCode.com>

	* Makefile.am (test_shortcut_bar_LDADD):
	Fixed references to eutil.

2000-02-20  Damon Chaplin  <damon@helixcode.com>

	* Makefile.am: added libetext.a and libeutil.a
	to test_shortcut_bar_LDADD, and got rid of e-icon-bar-text-item.[hc].
	I'll delete these soon.

	* e-icon-bar.c: changed to be a subclass of
	ECanvas and to use EText instead of EIconBarTextItem. Also set
	"width_set" & "height_set" of the GnomeCanvasPixbuf items so they
	work in the "Small Icon" view.

	* e-vscrolled-bar.c: use map/unmap instead of
	show/hide for the up & down buttons to avoid queueing resizes.
	Otherwise the scrolling starts off a bit jerky.

	* test-shortcut-bar.c: output a message when the
	main label is resized, just for info.

	* *.[hc]: updated my email address.

2000-02-07  Damon Chaplin  <damon@helixcode.com>

	* e-group-bar.c (e_group_bar_draw): finished.

	* e-icon-bar.c (e_icon_bar_item_released): 
	check that an item was pressed.

2000-01-26  bertrand  <bertrand@helixcode.com>

	* e-icon-bar.c (e_icon_bar_recalc_item_positions): 
	don't set the x_set and y_set 
	arguments as don't exist in gdk-pixbuf 
	anymore. 

2000-01-17  Chrsitopher James Lahey  <clahey@helixcode.com>
	
	* Makefile.am: Added gnomecanvaspixbuf to the
	list of libraries used.

2000-01-14  Christopher James Lahey  <clahey@helixcode.com>

	* e-shortcut-bar.c (e_shortcut_bar_add_item):
	Use e_bar_set_item_data.

	Drop item_url;  Require image argument;  Require user data
	argument. 

2000-01-09  Miguel de Icaza  <miguel@gnu.org>

	* e-icon-bar.c (e_icon_bar_init): Turn on anti-aliasing.

2000-01-08  Miguel de Icaza  <miguel@gnu.org>

	* e-icon-bar.c (e_icon_bar_add_item): Use
	GdkPixbuf here. 
	(e_icon_bar_get_item_image): ditto and initialize image.

	* e-shortcut-bar.c (e_shortcut_bar_add_item):
	Use gdk-pixbuf.

2000-01-09  Miguel de Icaza  <miguel@gnu.org>

	* e-icon-bar.c (e_icon_bar_init): Turn on anti-aliasing.

2000-01-08  Miguel de Icaza  <miguel@gnu.org>

	* e-icon-bar.c (e_icon_bar_add_item): Use
	GdkPixbuf here. 
	(e_icon_bar_get_item_image): ditto and initialize image.

	* e-shortcut-bar.c (e_shortcut_bar_add_item):
	Use gdk-pixbuf.

