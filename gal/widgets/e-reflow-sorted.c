/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * e-reflow-sorted.c
 * Copyright 2000, 2001, Ximian, Inc.
 *
 * Authors:
 *   Chris Lahey <clahey@ximian.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License, version 2, as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <config.h>

#include <math.h>
#include <string.h>

#include "gal/util/e-util.h"

#include "e-canvas.h"
#include "e-canvas-utils.h"
#include "e-reflow-sorted.h"

static void e_reflow_sorted_init		(EReflowSorted		 *card);
static void e_reflow_sorted_class_init	(EReflowSortedClass	 *klass);
static void e_reflow_sorted_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec);
static void e_reflow_sorted_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec);
static void e_reflow_sorted_dispose (GtkObject *object);
static void e_reflow_sorted_add_item(EReflow *e_reflow, GnomeCanvasItem *item, gint *position);

#define E_REFLOW_DIVIDER_WIDTH 2
#define E_REFLOW_BORDER_WIDTH 7
#define E_REFLOW_FULL_GUTTER (E_REFLOW_DIVIDER_WIDTH + E_REFLOW_BORDER_WIDTH * 2)

#define PARENT_TYPE E_REFLOW_TYPE
static EReflowClass *parent_class = NULL;

/* maximum insertions between an idle event that we will do without scheduling an idle sort */
#define ERS_INSERT_MAX (4)

/* The arguments we take */
enum {
	ARG_0,
	ARG_COMPARE_FUNC,
	ARG_STRING_FUNC
};



/* idle callbacks */

static int
compare_dereference (const void *p1, const void *p2, void *closure)
{
	GCompareFunc compare = closure;
	GnomeCanvasItem *item1 = *(GnomeCanvasItem **) p1;
	GnomeCanvasItem *item2 = *(GnomeCanvasItem **) p2;
	return compare (item1, item2);
}

static gboolean
ers_sort_idle(gpointer user_data)
{
	EReflowSorted *ers = user_data;
	EReflow *er = user_data;
	int length;
	int i;
	GnomeCanvasItem **items;
	GList *list_item;

	ers->sort_idle_id = 0;
	length = g_list_length (E_REFLOW(ers)->items);
	items = g_new (GnomeCanvasItem *, length);
	for (i = 0, list_item = er->items;
	     i < length;
	     i++, list_item = list_item->next) {
		items[i] = list_item->data;
	}
	e_sort (items, length, sizeof (GnomeCanvasItem *), compare_dereference, ers->compare_func);
	for (i = 0, list_item = er->items;
	     i < length;
	     i++, list_item = list_item->next) {
		list_item->data = items[i];
	}
	g_free (items);
	return FALSE;
}

static gboolean
ers_insert_idle(EReflowSorted *ers)
{
	ers->insert_count = 0;
	ers->insert_idle_id = 0;
	return FALSE;
}



E_MAKE_TYPE (e_reflow_sorted,
	     "EReflowSorted",
	     EReflowSorted,
	     e_reflow_sorted_class_init,
	     e_reflow_sorted_init,
	     PARENT_TYPE)

static void
e_reflow_sorted_class_init (EReflowSortedClass *klass)
{
	GObjectClass *object_class;
	EReflowClass *reflow_class;
	
	object_class = (GObjectClass*) klass;
	reflow_class = E_REFLOW_CLASS (klass);
	
	parent_class = g_type_class_ref (PARENT_TYPE);

	reflow_class->add_item = e_reflow_sorted_add_item;

	object_class->set_property  = e_reflow_sorted_set_property;
	object_class->get_property  = e_reflow_sorted_get_property;
	object_class->dispose  = e_reflow_sorted_dispose;

	g_object_class_install_property (object_class, PROP_COMPARE_FUNC,
					 g_param_spec_pointer ("compare_func",
							      _( "Compare func" ),
							      _( "Compare func" ),
							      G_PARAM_READWRITE));
	

	g_object_class_install_property (object_class, PROP_STRING_FUNC,
					 g_param_spec_pointer ("string_func",
							      _( "String func" ),
							      _( "String func" ),
							      G_PARAM_READWRITE));
}

static void
e_reflow_sorted_init (EReflowSorted *reflow)
{
	reflow->compare_func   = NULL;
	reflow->string_func    = NULL;
	reflow->sort_idle_id   = 0;
	reflow->insert_idle_id = 0;
	reflow->insert_count   = 0;
}

static void
e_reflow_sorted_set_property (GObject *object, guint prop_id,
			      const GValue *value, GParamSpec *pspec)
{
	GnomeCanvasItem *item;
	EReflowSorted *e_reflow_sorted;

	item = GNOME_CANVAS_ITEM (object);
	e_reflow_sorted = E_REFLOW_SORTED (object);
	
	switch (prop_id){
	case PROP_COMPARE_FUNC:
		e_reflow_sorted->compare_func = g_value_get_pointer (value);
		break;
	case PROP_STRING_FUNC:
		e_reflow_sorted->string_func  = g_value_get_pointer (value);
		break;
	}
}

static void
e_reflow_sorted_get_property (GObject *object, guint prop_id,
			      GValue *value, GParamSpec *pspec)
{
	EReflowSorted *e_reflow_sorted;

	e_reflow_sorted = E_REFLOW_SORTED (object);

	switch (prop_id) {
	case PROP_COMPARE_FUNC:
		g_value_set_pointer (value, e_reflow_sorted->compare_func);
		break;
	case PROP_STRING_FUNC:
		g_value_set_pointer (value, e_reflow_sorted->string_func);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
e_reflow_sorted_destroy (GtkObject *object)
{
	EReflowSorted *ers = E_REFLOW_SORTED (object);
	if (ers->sort_idle_id) {
		g_source_remove(ers->sort_idle_id);
		ers->sort_idle_id = 0;
	}
	if (ers->insert_idle_id) {
		g_source_remove(ers->insert_idle_id);
		ers->insert_idle_id = 0;
	}

	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static GList *
e_reflow_sorted_get_list(EReflowSorted *e_reflow_sorted, const gchar *id, int *position)
{
	int i = 0;
	if (e_reflow_sorted->string_func) {
		EReflow *reflow = E_REFLOW(e_reflow_sorted);
		GList *list;
		for (list = reflow->items; list; list = g_list_next(list), i++) {
			GnomeCanvasItem *item = list->data;
			char *string = e_reflow_sorted->string_func (item);
			if (string && !strcmp(string, id)) {
				if (position)
					*position = i;
				return list;
			}
		}
	}
	if (position)
		*position = -1;
	return NULL;
}

void
e_reflow_sorted_remove_item(EReflowSorted *e_reflow_sorted, const gchar *id, int *position)
{
	GList *list;
	GnomeCanvasItem *item = NULL;

	list = e_reflow_sorted_get_list(e_reflow_sorted, id, position);
	if (list)
		item = list->data;


	if (item) {
		EReflow *reflow = E_REFLOW(e_reflow_sorted);
		reflow->items = g_list_remove_link(reflow->items, list);
		g_list_free_1(list);
		g_object_unref (item);
		gtk_object_destroy(GTK_OBJECT(item));
		if ( GTK_OBJECT_FLAGS( e_reflow_sorted ) & GNOME_CANVAS_ITEM_REALIZED ) {
			e_canvas_item_request_reflow(GNOME_CANVAS_ITEM(e_reflow_sorted));
			e_reflow_post_add_item(E_REFLOW(e_reflow_sorted), NULL);
		}
	}
}

void
e_reflow_sorted_replace_item(EReflowSorted *e_reflow_sorted, GnomeCanvasItem *item, gint *old_pos, gint *new_pos)
{
	if (e_reflow_sorted->string_func) {
		char *string = e_reflow_sorted->string_func (item);
		e_reflow_sorted_remove_item(e_reflow_sorted, string, old_pos);
		e_reflow_sorted_add_item(E_REFLOW(e_reflow_sorted), item, new_pos);
	} else {
		if (old_pos)
			*old_pos = -1;
		if (new_pos)
			*new_pos = -1;
	}
}

GnomeCanvasItem *
e_reflow_sorted_get_item(EReflowSorted *e_reflow_sorted, const gchar *id, gint *position)
{
	GList *list;
	list = e_reflow_sorted_get_list(e_reflow_sorted, id, position);
	if (list)
		return list->data;
	else
		return NULL;
}

void
e_reflow_sorted_reorder_item(EReflowSorted *e_reflow_sorted, const gchar *id, gint *old_pos, gint *new_pos)
{
	GList *list;
	GnomeCanvasItem *item = NULL;

	list = e_reflow_sorted_get_list(e_reflow_sorted, id, old_pos);
	if (list)
		item = list->data;
	if (item) {
		EReflow *reflow = E_REFLOW(e_reflow_sorted);
		reflow->items = g_list_remove_link(reflow->items, list);
		g_object_unref (item);
		g_list_free_1(list);
		e_reflow_sorted_add_item(reflow, item, new_pos);
		e_canvas_item_request_reflow(GNOME_CANVAS_ITEM(e_reflow_sorted));
	} else {
		if (new_pos)
			*new_pos = -1;
	}
}

static void
e_reflow_sorted_add_item(EReflow *reflow, GnomeCanvasItem *item, gint *position)
{
	EReflowSorted *ers = E_REFLOW_SORTED(reflow);
	if (ers->compare_func) {
		if (ers->sort_idle_id == 0) {
			if (ers->insert_count > ERS_INSERT_MAX) {
				reflow->items = g_list_prepend (reflow->items, item);
				ers->sort_idle_id = g_idle_add_full(50, (GSourceFunc) ers_sort_idle, ers, NULL);
			} else {
				ers->insert_count++;
				if (ers->insert_idle_id == 0) {
					ers->insert_idle_id = g_idle_add_full(40, (GSourceFunc) ers_insert_idle, ers, NULL);
				}

				reflow->items = g_list_insert_sorted(reflow->items, item, ers->compare_func);
			}
		} else
			reflow->items = g_list_prepend (reflow->items, item);

		g_object_ref (item);

		if (position)
			*position = g_list_index(reflow->items, item);

		if ( GTK_OBJECT_FLAGS( ers ) & GNOME_CANVAS_ITEM_REALIZED ) {
			gnome_canvas_item_set(item,
					      "width", (double) reflow->column_width,
					      NULL);

			e_canvas_item_request_reflow(GNOME_CANVAS_ITEM(ers));
		}
	} else {
		if (position)
			*position = -1;
	}
	e_reflow_post_add_item(reflow, item);
}

void e_reflow_sorted_jump   (EReflowSorted *sorted,
			     GCompareFunc   compare_func,
			     void          *value)
{
	int columns = 0;
	EReflow *reflow = E_REFLOW(sorted);
	GList *list;
	GtkAdjustment *adjustment;

	for (list = reflow->columns; list; list = g_list_next(list)) {
		if (compare_func(((GList *)list->data)->data, value) >= 0) {
			GList *last = list->prev;
			if (last) {
				GList *walk;
				for (walk = last->data; walk != list->data; walk = g_list_next(walk)) {
					if (compare_func(walk->data, value) >= 0) {
						columns --;
						break;
					}
				}
			}
			adjustment = gtk_layout_get_hadjustment(GTK_LAYOUT(GNOME_CANVAS_ITEM(sorted)->canvas));
			gtk_adjustment_set_value(adjustment, (reflow->column_width + E_REFLOW_FULL_GUTTER) * columns);
			return;
		}
		columns ++;
	}
	columns --;
	adjustment = gtk_layout_get_hadjustment(GTK_LAYOUT(GNOME_CANVAS_ITEM(sorted)->canvas));
	gtk_adjustment_set_value(adjustment, (reflow->column_width + E_REFLOW_FULL_GUTTER) * columns);
}
