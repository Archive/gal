---------------------
gal-2.5.3  2005-06-7
--------------------

Bugzilla bugs fixed (see http://bugzilla.gnome.org/show_bug.cgi):

	#302154 - Cannot sort table items with gok (Li Yuan)

Other changes :

	Don't hide the tooltip if we don't have a canvas anymore.
	(Michael Zucchi)

Updated Translations :
	
	- vi (Clytie Siddall)


---------------------
gal-2.5.2  2005-05-17
--------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#260312 - gettext won't accept NULL input string [Solaris] (Suresh Chandrasekharan)
	#302275 - crash after delete all the contact in local folder.  (Mengjie Yu)
	#301888 - e-table background doesn't compatible with theme settings (Li Yuan)
	#74181  - F6 fails to jump out of task list. (Li Yuan)	

Other changes :
	Port to Windows - Initial commits (Tor Lillqvist)

Updated Translations :
	et (Kostas Papadimas)

----------------------
gal-2.5.0  2005-04-26
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#300954 - Window will not follow the focused contact when browsing multiple contacts (Mengie Yu)
	#274434 - Evolution crashes when pressing space key after deleting the last e-mini-card in Contact part (Mengie Yu)
	#240762 - Cannot edit the '_Address Cards' View (Li Yuan)

Other bugs and changes:
	
	- Updated translations: 
	et (Ivar Smolin)
	ne (Pawan Chitrakar)

----------------------
gal-2.4.0  2005-03-07
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#72793 - Grab Focus of table cells in grabs the wrong for a11y (Harry Lu)
	#73009 - Mnemonics not working in fields window (Li Yuan)

Other bugs and changes:
	
	- Updated translations: 
	hi (Rajesh Ranjan)
	sr (Danilo Segan)
	sr@Latn (Danilo Segan)
	hu (Laszlo Dvornik)
	ro (Dan Damian)

----------------------
gal-2.3.5  2005-02-28
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#72793 - Grab Focus of table cells in grabs the wrong for a11y (Harry Lu)
	#73009 - Mnemonics not working in fields window (Li Yuan)

Other bugs and changes:
	
	- Updated translations: 
	de (Hendrik Richter)
	el (Kostas Papadimas, Nikos Charonitakis)
	ca (Gil Forcada)
	ru (Leonid Kanter)
	da (Martin Willemoes Hansen)
	cy (Rhys Jones)
	gu (Ankit Patel)
	fr (Christophe Merlet)
	pl (Artur Flinta)
	pt_BR (Raphael Higino)
	uk (Maxim Dziumanenko)
	pt (Duarte Loreto)
	fi (Ilkka Tuohela)
	nb (Kjartan Maraas)
	no (Kjartan Maraas)
	bg (Vladimir Petkov)
	sq (Laurent Dhima)
	lt (Zygimantas Berucka)
	en_GB (David Lodge)
	nl (Vincent van Adrighem)
	ja (Takeshi AIHANA)
	ko (Changwoo Ryu)
	cs (Miloslav Trmac)
	sk (Marcel Telka)
	es (Francisco Javier F. Serrador)
	en_CA (Adam Weinberger)
	sv (Christian Rose)
	et (Priit Laes)

----------------------
gal-2.3.4  2005-02-07
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#70261 - No a11y output when input task summary in task table (Li Yuan)
	#61688 - Settings dialog's left navigation pane can't read by gnopernicus (Li Yuan)
	#48976 - evolution crashed when click tools through gok (Li Yuan)
	#68675 - The state 'FOCUSED' remains even if the cell is not the current focused one (Li Yuan)
	#70324 - Evolution Mail is Slow in a11y mode (Li Yuan)
	#68681 - 'Focus' event- redundant in case of moving between tree table lines (Li Yuan)
	#68627 - Task table cells should be "focusable" befor "focused" (Li Yuan)
	#71158 - Can't UI Grab the etree (Li Yuan)
	#38195 - tasks list - i18n problem in Priority column (JP)

Other bugs and changes:
	
	- Updated translations: 
	ru (Leonid Kanter)
	en_GB (David Lodge)
	en_CA (Adam Weinberger)
	sk (Marcel Telka)
	cs (Miloslav Trmac)
	lt (Žygimantas Beručka)
	es (Francisco Javier F. Serrador)

----------------------
gal-2.3.3  2005-01-24
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#9413 - Tooltips when hovering over text in card view is wrong colour

Other bugs and changes:
	
	- Updated translations: 
	et (Priit Laes)
	sk (Marcel Telka)
	it (Alessio Frusciante)

----------------------
gal-2.3.2  2005-01-10
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#29309 - Adding a new column leaves the window open

Other bugs and changes:
	
	- Updated translations: 
	zh_CN (Funda Wang)

----------------------
gal-2.3.1  2004-11-28
----------------------

Other bugs and changes:
	
	-don't lose focus when rebuilding categories (Hao Sheng)
	- Updated translations: 
	de (Hendrik Richter)

----------------------
gal-2.3.0  2004-11-01
----------------------

Other bugs and changes:

	- bumped API to 2.4 and version to 2.3

----------------------
gal-2.2.3  2004-10-01
----------------------

Other bugs and changes:

	- Updated translations: 
	 en_CA (Adam Weinberger)
	 fr (Craig Jeffares)

----------------------
gal-2.2.2  2004-09-23
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#637731 - blocking mouse grab when autocompleting (Suresh)

Other bugs and changes:

	- Updated translations: 
	 it (Craig Jeffares)
	 zh_TW (Craig Jeffares)
	 id (Mohammad DAMT)
	 th (Supranee Thirawatthanasuk)
 	 hi (Guntupalli Karunakar)

----------------------
gal-2.2.1  2004-09-10
----------------------

Other bugs and changes:

	- Updated translations: 
 	tr (Baris Cicek)
	el (Nikos Charosnitakis, Kostas Papadimas)
	ar (Arafat Medini)
	en_CA (Adam Weinberger)
	nn (Åsmund Skjæveland)
	cs (Miloslav Trmac)
	bg (Vladimir Petkov)
	it (Luca Ferretti)
	de (Hendrik Richter)
	ru (Leonid Kanter)
	bn (Runa Bhattacharjee)

----------------------
gal-2.2.0  2004-08-27
----------------------

----------------------
gal-2.2.0  2004-08-27
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#45931 - blocking mouse grab when autocompleting (Toshok)

Other bugs and changes:

	- remove 2.0 pkg-config file (JP)
	- Updated translations: eu, ca, nb, sq, uk, da, fi, az

----------------------
gal-2.1.14  2004-08-13
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#61932 - e-table popup in wrong spot (Zucchi)
	#61936 - set etable config dialog parent and position and save edits (Radek)

Other bugs and changes:

	- e-entry fixes (Toshok)
	- Updated translations: gu, pa, sr, ko, pt


----------------------
gal-2.1.13  2004-08-02
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#57914 - don't allow deletion of builtin views (Radek)

Other bugs and changes:

	- Updated translations: bn, no, sq

----------------------
gal-2.1.12  2004-07-19
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#57914 - don't allow deletion of builtin views (Radek)

Other bugs and changes:

	- delete all selected views in dialog (Larry)
	- properly freeze/unfreeze a sorted table (Radek)
	- Updated translations: sv, es, ja, sq, 

----------------------
gal-1.99.9  2003-07-28
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#46380 - crash in Tasks Define View field picker. (Mike)
	#44904 - crash in e_utf8_strftime (Mike)

Other bugs and changes:

	- remove EPaned, EVPaned, EHPaned, EVScrollbar, EHScrollbar, and EScrollFrame
	- leak fixes (DanW and Chris)
	- Updated translations: ms (Hasbullah Bin Pit), nl (Vincent van Adrighem), pl (Artur Flinta)


----------------------
gal-1.99.8  2003-06-25
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#42861 - shortcut bar scroll arrow buttons too big. (Mike)
	#43523 - Crash on etree search without a cursor. (Mike)
	#43893 - Crash on startup in etree. (Mike)
	#43989 - libtool version check failure. (Elijah Newren)
	#44258 - Block the disabled columns in Define Views. (Mike)
	#44610 - alternate row color was sorta purplish. (Mike)

Other bugs and changes:

	- leak fixes (Larry)
	- Updated Translations: da (Ole Larson), es (Gerardo J Seguin Giraldez & Francisco Javier F. Serrador), fi (Pauli Virtanen), hu (Andras Timar), no (Kjarten Maraas), pt_BR (Gustavo Maciel Dias Vieira), ta (Abel Cheung), zh_CN (Funda Wang), zh_TW (Abel Cheung)


----------------------
gal-1.99.7  2003-06-02
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#43485 - Raw utf8 in etable date cells. (Mike)
	#43727 - View state not persistent for some account names. (Mike)

Other bugs and changes:

	- Updated Translations: be (Belarusian team), nl (Vincent van Adrighem)
	no (Kjartan Maraas), pt (Duarte Loreto), ru (Russian Team)


----------------------
gal-1.99.6  2003-05-21
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#40896 - Crash on selection based actions. (Mike)
	#40996 - Combo cells require double click to select item.(Mike)
	#41158 - Incorrect button ordering on categories dialog. (Toshok)
	#41217 - HIG changes to Edit Categories dialog. (Anna)
	#41698 - Folder displays scrolled down from top. (Mike)
	#42156 - Return statement in e_completion_callbacks_new. (Mike)
	#42188 - "To:" and "CC:" don't accept zh_CN characters. (Toshok)
	#42395 - DragnDrop issues with the shortcut-bar. (Mike)
	#42478 - make clean fix. (Mike)
	#42493 - sgml formatting fixes in docs. (Alastair Tse)
	#42856 - Crash in etree updating nodes. (Mike)
	#42608 - Categories master list dialog was only accessible once. (Toshok)
	#42617 - crash dragging column headers in etable. (Mike)
	#42622 - crash while editing text cells in etable. (Mike)
	#42952 - scroll adjustment jumps on resize of table. (Ettore)
	#43153 - crash creating new custom views. (Mike)
	#43199 - crash in pango layout code for text cells. (Mike)


Other bugs and changes:

	- leaks plugged (JP)
	- fix destruction of torn off combos.	(Jody)
	- bring spec file up to date. (Rui Miguel Seabra)
	- cygwin build fixes (Masahiro Sakai)
	- translation updates: cs (Miloslav Trmac), de (Christian Neumair), pt (Duarte Loreto),
		sr (Danilo Šegan), sv (Christian Rose)

----------------------
gal-1.99.4  2003-04-28
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#37894 - ETable text cell justification. (Mike)
	#40906 - Button ordering in GalView dialogs  (Mike)
	#41049 - Define Views menuitem needs ... (Mike)
	#41051 - DefineViews dialog cleanup. (Mike)
	#41054 - DefineViews dialog use stock buttons. (Mike)
	#41055 - DefineViews dialog title. (Mike)
	#41056 - DefineViews dialog padding/spacing. (Mike)
	#41456 - Crash on evo startup (Mike)
	#41497 - index guarding for ETableSubset (JP)
	#41498 - show elipses if text cells overflow (Mike)
	#41892 - don't wrap/ellipsify layout if editing (Mike)

Other bugs and changes:

	- leaks plugged (Hans Petter)
	- spec file updates (Rui Miguel Silva Seabra)
	- fixed EBitArray's update-selection-on-delete logic (Chris)
	
----------------------
gal-1.99.3  2003-04-07
----------------------

Bugzilla bugs fixed (see http://bugzilla.ximian.com/show_bug.cgi):

	#38320 - etable crash. (Mike and Antonio Xu)
	#38855 - Can't scroll in shortcut-bar. (Mike)
	#39175 - Forte compiler fixes (Henry Jia)
	#39176 - Forte compiler fixes (Alex Jiang)
	#39212 - Shift-up/down selection doesn't work. (Mike)
	#39411 - etree crash on sort. (Mike)
	#39508 - EText doesn't scroll. (Chris)
	#39702 - EText doesn't allow accented char entry (Chris)
	#39717 - popus focus stealing. (Michael)
	#39801 - sort indicators inconsistent with treeview (Mike)
	#39856 - Redraw problems on column resize. (Chris)
	#39896 - GalView sorts not persistent. (Mike)
	#40074 - Threads always open all expanded. (Mike)
	#40393 - etree likes to collapse folders. (Mike)
	#40753 - Shortcut bar initial scrolled to bottom. (Mike)
	#40906 - GalView and ETable dialog HIG button order. (Mike)

Other bugs and changes:

	- Added po back into the build (Mike)
	- Fix to alias gb-2312 charset to the known gb2312 (Jeff)
	- libxml2 updates to e_xml (Jeff)
	- e_notice removed and moved into evolution (Dan)
	- removed automake-1.6-isms (Mike)
	- versioned .mo files (Mike)
	- completed porting of EText to 2.0 (Chris)
	- ECanvas support of IMContext (Larry)
	- added underline column to etable (Chris)
	- dnd fixes (Ettore and Mike)
	- define views dialog crash fix (Hans Petter)
	- updated translations: Kostas Papadimas (el), 
	Kjartan Maraas (no), Pablo Gonzalo del Campo (es),
	Metin Amiroff (az)
	
----------
Gal-2
----------

    Interface changes
    - color_combo now takes the more general GdkPixbuf in place of xpm
      data.
    - pixmap_combo also takes inline GdkPixbufs

-----------------------------------------------------------------------

0.8
    Almer	Simplify the colour combo/palette/group relationship.
    Jody	Add instance code to colour groups.

New stuff in gal-0.4.1 (since gal-0.2)

  - Lots of bug fixes.

  - Tooltips in ETable behave more nicely.

  - Stronger font handling.

  - Lots more translations.

  - ETree is much faster now.

  - ETable headers are much prettier now.

  - Sorting in ETree works much better now.
